	  subroutine hg_init 

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    This subroutine computes variables related to the watershed hydrology:
!!    the time of concentration for the subbasins, lagged surface runoff,
!!    the coefficient for the peak runoff rate equation, and lateral flow travel
!!    time.


	  use parm

	  real :: hgco
	  
	  hgco =     0.85
	  hgpercco = 0.06

	 !!Initial soil contents of Mercury
	  Hg_soil_layer(1,1,:,:) = 90.*hgco
	  Hg_soil_layer(2,2,:,1) = 8549.1*hgco
	  Hg_soil_layer(1,2,:,:) = 0.9*hgco
	  Hg_soil_layer(2,3,:,1) = 359.9*hgco
	  Hg_soil_layer(1,1,:,:) = 0
 

	  return
	  end