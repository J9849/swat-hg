	  subroutine hg_init 

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    This subroutine computes variables related to the watershed hydrology:
!!    the time of concentration for the subbasins, lagged surface runoff,
!!    the coefficient for the peak runoff rate equation, and lateral flow travel
!!    time.


	  use parm

	  integer:: jres
        real*8 :: hgco,sed_por_vol,sed_sol
        
        bulk_density = sum(sub_bd(:)) / msub
	  
	  hgco = 1.1
	  hgpercco = 0.15

   !----------------------------------------------------------------------------------------------------   
   ! Hg_soil_layer(a,b,c,d) a:dissolved(1)/particulate(2); b:Hg0(1)/Hg2+(2)/MeHg(3); c:ihru; d:layer
   !----------------------------------------------------------------------------------------------------   
	 !!Initial soil contents of Mercury
	  Hg_soil_layer(1,1,:,:) = 90.*hgco
	  Hg_soil_layer(2,2,:,1) = 8549.1*hgco
	  Hg_soil_layer(1,2,:,:) = 0.9*hgco
	  Hg_soil_layer(2,3,:,1) = 359.9*hgco
	  Hg_soil_layer(1,1,:,:) = 0
        
        do j=1,nhru
         if (cpnm(idplt(j))/='FRSD'.and.cpnm(idplt(j))/='FRSE'
     &         .and.cpnm(idplt(j))/='FRST') then
 	          Hg_soil_layer(:,:,j,:) = Hg_soil_layer(:,:,j,:) * 0.5
         endif
        end do
             
 
        !reservoir initial condition based on Jangsung Lake sample data
        do jres=1, nres
            ressa = br1(jres) * res_vol(jres) ** br2(jres)             !reservoir water surface area, ha
            if (res_vol(jres)>0.1.and.ressa>0.001) then
                resdepth = res_vol(jres) / (ressa*1e4) !water depth, m 
            else
                resdepth = 0.
            endif
            
            Hg_resstor(1,2,jres,1)=0.92*1e-3*res_vol(jres) !dissolved Hg2+ mg
            Hg_resstor(1,3,jres,1)=0.04*1e-3*res_vol(jres) !dissolved MeHg mg
            Hg_resstor(2,2,jres,1)=0.35*1e-3*res_vol(jres) !particulate Hg2+ mg
            Hg_resstor(2,3,jres,1)=0.15*1e-3*res_vol(jres)  !particulate MeHg mg      

            !sediment layer1
            sed_por_vol = ressa*1e4 * 0.05 * 0.6 !m3 5cm layer thickness, porosity=0.6
            sed_sol = bulk_density * 0.4 * ressa*1e4 * 0.05  !tons soil
            Hg_resstor(1,2,jres,2) = 12.2*1e-3*sed_por_vol !dissolved Hg2+ in 5cm sediment layer mg
            Hg_resstor(1,3,jres,2) = 2.45*1e-3*sed_por_vol !dissolved MeHg in 5cm sediment layer mg
            Hg_resstor(2,2,jres,2) = 160.8 * sed_sol  !particulate Hg2+ in 5cm sediment layer mg
            Hg_resstor(2,3,jres,2) = 1.2 * sed_sol  !particulate MeHg in 5cm sediment layer mg        
            Hg_resstor(1,2,jres,3) = Hg_resstor(1,2,jres,2)
            Hg_resstor(1,3,jres,3) = Hg_resstor(1,3,jres,2)
            Hg_resstor(2,2,jres,3) = Hg_resstor(2,2,jres,2)
            Hg_resstor(2,3,jres,3) = Hg_resstor(2,3,jres,2)  
	  end do
        return
	  end